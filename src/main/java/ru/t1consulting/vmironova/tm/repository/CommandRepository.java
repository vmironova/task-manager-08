package ru.t1consulting.vmironova.tm.repository;

import ru.t1consulting.vmironova.tm.api.ICommandRepository;
import ru.t1consulting.vmironova.tm.constant.ArgumentConst;
import ru.t1consulting.vmironova.tm.constant.TerminalConst;
import ru.t1consulting.vmironova.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    public static Command INFO = new Command(
            TerminalConst.INFO, ArgumentConst.INFO,
            "Show memory info."
    );

    public static Command ABOUT = new Command(
            TerminalConst.ABOUT, ArgumentConst.ABOUT,
            "Show developer info."
    );

    public static Command VERSION = new Command(
            TerminalConst.VERSION, ArgumentConst.VERSION,
            "Show application version."
    );

    public static Command HELP = new Command(
            TerminalConst.HELP, ArgumentConst.HELP,
            "Show application commands and arguments."
    );

    public static Command ARGUMENTS = new Command(
            TerminalConst.ARGUMENTS, ArgumentConst.ARGUMENTS,
            "Show application arguments."
    );

    public static Command COMMANDS = new Command(
            TerminalConst.COMMANDS, ArgumentConst.COMMANDS,
            "Show application commands."
    );

    public static Command EXIT = new Command(
            TerminalConst.EXIT, null,
            "Close application."
    );

    private static Command[] TERMINAL_COMMANDS = new Command[]{
            INFO, ABOUT, VERSION, HELP, ARGUMENTS, COMMANDS, EXIT
    };

    @Override
    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}
